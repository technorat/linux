# Команды Linux

## Система
| Команда | Описание | Примечание |
|---|---|---|
| swapoff -a | Отключить swap | Работает до перезагрузки. На постоянной основе нужно удалять из файла /etc/fstab |
| hostnamectl set-hostname node1.internal | Установить hostname | Работает не во всех дистрах, универсально - вносить правки в /etc/hostname вручную |
| free -m | Посмотреть сколько памяти и свопа на машине в мегабайтах | |
| dmesg \| grep -i iwlagn | | |
| sysctl --system | Применить изменения параметров в файлах /etc/sysctl.d/* без перезагрузки | |
| sysctl kernel.sysrq=1 | Активировать kernel.sysrq до перезагрузки | Для постоянных изменений вносим в /etc/sysctl.d/* |
| echo "1" > /proc/sys/kernel/sysrq | Активировать kernel.sysrq до перезагрузки | Для постоянных изменений вносим в /etc/sysctl.d/* |


## Управление процессами и службами/демонами
| Команда | Описание | Примечание |
|---|---|---|
| systemctl status -l kubelet | Показывает статус службы не в урезанном виде, а полные строки | |
| sysctl -a | | |
| systemctl daemon-reload | | |
| sysctl -p | | | Чтобы сделать изменение постоянным после перезагрузки:
| systemctl show-environment | | |
| systemctl set-environment NO_PROXY=127.0.0.1,localhost,cluster2-172-21-48-17,172.21.48.17 | | |
| journalctl -xe | | |
| journalctl -ukubelet | | |
| top | | |
| htop | | |
| strace | | |
| pstree | | |
| fuser | Утилита показывает какие процессы используют файл | |
| systemctl list-units --type service --state running | | |

## Работа с ПО, пакетами, пакетные менеджеры
| Команда | Описание | Примечание |
|---|---|---|
| dpkg -l *netplan* | | |


## Работа с сетью
| Команда | Описание | Примечание |
|---|---|---|
| netstat -s | | |
| netstat -tunlp | | |
| du | | |
| df | | |
| rsync -av --delete --ignore-existing /path/to/local/folder/ /path/to/remote/folder/ | Сравнивает текущую папку с удаленной папкой на сервере “example.com”: | |
| resolvconf | | |
| ip route show | | |
| netplan apply | | |
| ip link show | | |
| ifconfig ens34 up | | |
| nmcli con show  | покажет все доступные интерфейсы | |
| nmcli con up/down ИМЯ ИНТЕРФЕЙСА  | поднять/уронить интерфейс | | 
| ssh-keygen -f "/root/.ssh/known_hosts" -R "10.125.6.21" | | |
| iwconfig | | |

## Диски
| Команда | Описание | Примечание |
|---|---|---|
| lsblk | Инфа о дисках и разделах | |

## Железо
| Команда | Описание | Примечание |
|---|---|---|
| lshw -class network | | |
| lspci | | |




